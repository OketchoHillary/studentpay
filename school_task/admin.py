import json
from datetime import datetime, timedelta

import pytz
from django.contrib import admin
from django.contrib.admin import AdminSite
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Count
from django.views.decorators.cache import never_cache
from rangefilter.filter import DateRangeFilter

from school_task.models import SchoolAuth, Transactions, AccessToken
ind = pytz.timezone('Africa/Kampala')


class TaskAdminSite(AdminSite):
    index_template = 'admin/index.html'
    site_title = 'School Monitor'

    @never_cache
    def index(self, request, extra_context=None):
        extra_context = extra_context or {"chart_data": 'I am Me'}

        # Call the superclass changelist_view to render the page
        return super().index(request, extra_context=extra_context)


admin_site = TaskAdminSite()


class TransactionAdmin(admin.ModelAdmin):
    list_display = ['student_name', 'school_code', 'transaction_status', 'amount', 'timestamp', 'transfer_status']
    list_filter = [('timestamp', DateRangeFilter), 'transaction_status', 'transfer_status']
    search_fields = ['student_name', 'student_reg_number']
    list_per_page = 50
    save_as = True
    save_on_top = True
    change_list_template = 'admin/change_list_graph.html'

    def changelist_view(self, request, extra_context=None):
        today = datetime.now().date()

        y = Transactions.objects.extra(select={'date': 'date( timestamp )'}).values('date')\
            .annotate(y=Count('timestamp')).filter(timestamp__gt=today-timedelta(days=7))
        days = []
        trans_count = []
        for key in y:
            d = key['date']
            a = key['y']
            days.append(d)
            trans_count.append(a)

        as_json = json.dumps(list(y), cls=DjangoJSONEncoder)
        schools = Transactions.objects.all().distinct().values('school_code').count()

        extra_context = extra_context or {"school": schools, "chart_data": as_json}

        # Call the superclass changelist_view to render the page
        return super().changelist_view(request, extra_context=extra_context)


class SchoolsAdmin(admin.ModelAdmin):
    list_display = ['name', 'code']
    search_fields = ['name', 'code']
    # readonly_fields = ['password']
    list_per_page = 50


class TokenAdmin(admin.ModelAdmin):
    list_display = ['user', 'token']
    readonly_fields = ['token']


admin.site.register(Transactions, TransactionAdmin)
admin.site.register(SchoolAuth, SchoolsAdmin)
admin.site.register(AccessToken, TokenAdmin)
