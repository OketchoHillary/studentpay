from django.apps import AppConfig


class SchoolTaskConfig(AppConfig):
    name = 'school_task'
