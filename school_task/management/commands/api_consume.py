import datetime
import json
from hashlib import md5

import requests
from django.core.mail import send_mail
from django.core.management.base import BaseCommand
from requests.adapters import HTTPAdapter
from urllib3 import Retry

from school_task.models import SchoolAuth
from school_task.utils import transaction_mgt


class Command(BaseCommand):

    help = 'School Pay Task'

    def handle(self, *args, **options):

        schools = SchoolAuth.objects.all()

        for school in schools:
            current_date = datetime.datetime.today()
            convert = current_date.strftime('%Y-%m-%d')
            school_pass = school.password
            school_code = school.code
            school_value = '{}{}{}'.format(school_code, convert, school_pass)
            encode_value = school_value.encode('utf-8')
            convert_encoded = md5(encode_value).hexdigest()
            print(convert_encoded)
            try:
                url = 'https://schoolpay.co.ug/paymentapi/AndroidRS/SyncSchoolTransactions/' \
                      '{}/{}/{}'.format(school_code, convert, convert_encoded)
                session = requests.Session()
                retry = Retry(connect=3, backoff_factor=0.5)
                adapter = HTTPAdapter(max_retries=retry)
                session.mount('http://', adapter)
                session.mount('https://', adapter)

                r = session.get(url)

                loaded_json = json.loads(r.text)
                data = loaded_json['transactions']

                # picking the days transactions
                transaction_mgt(data, school.code)
            except Exception as e:
                send_mail(
                    'SchoolPay API Consumption error',
                    str(e),
                    'ceo@nugsoft.com',
                    ['larryoketcho@gmail.com', 'dudi7046@gmail.com', 'ceo@nugsoft.com'],
                    fail_silently=False,
                )

            finally:
                print('Done')


