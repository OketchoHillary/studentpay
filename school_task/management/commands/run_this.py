import time

from django.core.management.base import BaseCommand
import schedule


class Command(BaseCommand):

    help = 'Tossapp lotto'

    def handle(self, *args, **options):
        def job():
            print("I'm working...")

        schedule.every(10).seconds.do(job)

        while True:
            schedule.run_pending()
            time.sleep(1)
