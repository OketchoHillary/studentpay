import uuid

from django.contrib.auth.models import User
from django.db import models


class Transactions(models.Model):
    amount = models.IntegerField(null=True)
    payment_date = models.DateTimeField(null=True)
    school_pay_receipt_number = models.IntegerField(null=True)
    source_channel_transaction_id = models.CharField(null=True, max_length=250)
    source_channel_transaction_detail = models.CharField(null=True, max_length=250)
    source_payment_channel = models.CharField(max_length=250, null=True, blank=True)
    student_name = models.CharField(max_length=250, null=True)
    student_payment_code = models.IntegerField(null=True)
    student_reg_number = models.IntegerField(null=True, blank=True)
    transaction_status = models.CharField(max_length=50, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    transfer_status = models.BooleanField(default=False)
    school_code = models.IntegerField(null=True)

    class Meta:
        db_table = 'transactions'
        verbose_name = 'Transaction'


class SchoolAuth(models.Model):
    name = models.CharField(max_length=250, blank=True, null=True)
    code = models.IntegerField(null=True)
    password = models.CharField(max_length=250, null=True)

    class Meta:
        db_table = 'SchoolAuth'


class AccessToken(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    token = models.UUIDField(default=uuid.uuid4)
