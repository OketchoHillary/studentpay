from rest_framework import serializers

from school_task.models import Transactions


class TransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transactions
        fields = '__all__'