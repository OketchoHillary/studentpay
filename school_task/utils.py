from school_task.models import Transactions


def transaction_mgt(tr, sc):
    for obj in tr:
        if len(Transactions.objects.filter(source_channel_transaction_id=obj['sourceChannelTransactionId'],
                                           school_pay_receipt_number=obj['schoolpayReceiptNumber'], school_code=sc)) > 0:
            pass
        else:
            Transactions.objects.create(amount=obj['amount'], payment_date=obj['paymentDateAndTime'],
                                        school_pay_receipt_number=obj['schoolpayReceiptNumber'],
                                        source_channel_transaction_detail=obj['sourceChannelTransDetail'],
                                        source_channel_transaction_id=obj['sourceChannelTransactionId'],
                                        source_payment_channel=obj['sourcePaymentChannel'],
                                        student_name=obj['studentName'], student_payment_code=obj['studentPaymentCode'],
                                        transaction_status=obj['transactionCompletionStatus'], school_code=sc)