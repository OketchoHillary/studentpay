from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets

from rest_framework.views import APIView

from school_task.models import Transactions, AccessToken
from school_task.serializers import TransactionSerializer


class TransactionView(APIView):

    def get(self, request):
        queryset = Transactions.objects.all()
        return Response({'transactions': TransactionSerializer(queryset, many=True).data}, status=status.HTTP_200_OK)


class ViewTransaction(viewsets.ViewSet):
    permission_classes = ()
    authentication_classes = ()

    def get_transactions(self, request, code, token):
        # accessor = get_object_or_404(AccessToken, token=token)
        try:
            if AccessToken.objects.filter(token=token).exists():
                queryset = Transactions.objects.filter(transfer_status=False, school_code=code)
                return Response({'transactions': TransactionSerializer(queryset, many=True).data},
                                status=status.HTTP_200_OK)
            else:
                return Response({'error': 'Invalid Token'}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'error': str(e)})


class GetLastValue(viewsets.ViewSet):
    permission_classes = ()
    authentication_classes = ()

    def last_value(self, request, pk, code, token):
        try:
            if AccessToken.objects.filter(token=token).exists():
                x = Transactions.objects.filter(id__lte=pk, transfer_status=False, school_code=code)
                with transaction.atomic():
                    counter = 0
                    for transfer in x:
                        transfer.transfer_status = True
                        counter += 1
                        transfer.save()
                    print(counter)
                # queryset = Transactions.objects.filter(id__gte=pk).update(transfer_status=True)

                return Response({'transactions': 'Successfully updated {} items'.format(counter)},
                                status=status.HTTP_200_OK)
            else:
                return Response({'error': 'Invalid Token'}, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'error': str(e)})
